echo " ------ SkyTuring going to IMAV ------"
pkill rosrun
pkill roslaunch
pkill roscore

wait

./setup_ws.sh

gnome-terminal --geometry=30x30+100+100 \
\
--tab -e 'bash -c "sleep 0;\
 roslaunch mavros px4.launch fcu_url:="udp://:14550@127.0.0.1:14557";\
 exit;\
 exec bash"' \
\
--tab -e 'bash -c "sleep 1;\
 cd ~/Firmware/ ;\
 source ~/catkin_ws/devel/setup.bash;\
 source Tools/setup_gazebo.bash $(pwd) $(pwd)/build/posix_sitl_default;\
 export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:$(pwd);\
 export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:$(pwd)/Tools/sitl_gazebo;\
 roslaunch px4 my_launch.launch;\
 exit;\
 exec bash"' \
\
--tab -e 'bash -c "sleep 2;\
 rosrun dronecontrol2 controle.cpp;\
 exit;\
 exec bash"' \

while [ "$key" != 'q' ]
do
    read -n1 -r -p "Press q to exit simulation..." key
done
echo "exiting simulation"
pkill rosrun
pkill controle.cpp
pkill roslaunch
pkill roscore

wait
