if [ -e ~/Firmware ]
then
   echo "OK - o diretório do firmware esta correto"
else
   cd ~/
   git clone https://github.com/PX4/Firmware.git
fi

if [ -e ~/Firmware/Tools/sitl_gazebo/worlds/my_worlds ]
then
    echo "OK - o link para my_gazebo_worlds ja foi criado"
else
    cd ~/Firmware/Tools/sitl_gazebo/worlds/
    ln -s ~/catkin_ws/my_gazebo_worlds my_worlds
    echo "OK - link criado em ~/Firmware/Tools/sitl_gazebo/worlds/    para   ~/catkin_ws/my_gazebo_worlds"
fi


if [ -e ~/Firmware/launch/my_launch.launch ]
then
    rm ~/Firmware/launch/my_launch.launch 
    cp ~/catkin_ws/launch/my_launch.launch ~/Firmware/launch/ 
    echo "OK - my_launch foi atualizado em ~/Firmware/launch"
else 
    cp ~/catkin_ws/launch/my_launch.launch ~/Firmware/launch/
    echo "OK - my_launch foi adicionado em ~/Firmware/launch"
fi



if [ -e ~/Firmware/Tools/sitl_gazebo/models/iris_original ]
then
    echo "OK - backup de iris_original ja existe"
else
    mv ~/Firmware/Tools/sitl_gazebo/models/iris ~/Firmware/Tools/sitl_gazebo/models/iris_original
fi


if [ -e ~/catkin_ws/my_gazebo_models/iris ]
then
    rm -r ~/Firmware/Tools/sitl_gazebo/models/iris
    cp -r ~/catkin_ws/my_gazebo_models/iris ~/Firmware/Tools/sitl_gazebo/models/
    echo "OK - o modelo iris foi atualizado na pasta ~/Firmware/Tools/sitl_gazebo/models/"
fi

if [ -e ~/.gazebo/models/ ]
then
    if [ -e ~/camera_ros/ ]
    then
        echo "OK - camera ros esta em .gazebo models"
    else
        cp -r ~/catkin_ws/my_gazebo_models/camera_ros/ ~/.gazebo/models/
        echo "OK - modelo camera_ros copiado para a pasta ~/.gazebo/models/"
    fi
else
    echo "ERROR - pasta ~/.gazebo/ nao encontrada, para seguir com o setup da simulação confira se o gazebo e o ros estão instalados"
fi

echo " "
echo "SkyTuring  rocks!!!"
