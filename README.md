#catkin_ws

###Instalações necessarias:
[ros](http://wiki.ros.org/kinetic/Installation/Ubuntu)

[Mavros](https://dev.px4.io/en/ros/mavros_installation.html)

[Gazebo ros](http://gazebosim.org/tutorials?tut=ros_installing)

* como preparar o work space:
1. `cd ~/`
2. 
3. `cd ~/catkin_ws`
4. `catkin build`
5. `source devel/setup.bash`
6. `./simulate.sh`          


![alt text]( https://miro.medium.com/fit/c/240/240/1*MW70Iy2PSwLJAkD5zzuqNg.png "Grupo Turing - pesquisa em inteligência artificial" | height=100)
![alt text]( https://scontent-gru2-1.xx.fbcdn.net/v/t1.0-9/22815228_298404710662212_3665587368084050387_n.png?_nc_cat=0&oh=ed8030d68a71d127b1cf8ca2c3c31b9f&oe=5BB83589 "SkyRats - desenvolvimento de micro aeronaves" | height=100)
