#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/arthur/catkin_ws/devel/.private/catkin_tools_prebuild:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/opt/ros/kinetic/lib:/opt/ros/kinetic/lib/x86_64-linux-gnu:/usr/local/cuda-8.0/lib64:/usr/local/cuda-8.0/lib64"
export PKG_CONFIG_PATH="/opt/ros/kinetic/lib/pkgconfig:/opt/ros/kinetic/lib/x86_64-linux-gnu/pkgconfig"
export PWD="/home/arthur/catkin_ws/build/catkin_tools_prebuild"
export PYTHONPATH="/opt/ros/kinetic/lib/python2.7/dist-packages"
export ROSLISP_PACKAGE_DIRECTORIES="/home/arthur/catkin_ws/devel/.private/catkin_tools_prebuild/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/arthur/catkin_ws/build/catkin_tools_prebuild:/opt/ros/kinetic/share:/home/arthur/catkin_ws/src/dronecontrol2"