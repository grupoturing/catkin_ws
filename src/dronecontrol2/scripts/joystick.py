#!/usr/bin/env python

import rospy

from geometry_msgs.msg import PoseStamped

from std_msgs.msg import String

##TEM ALGUMA COISA CAGADEX AQUI #####

###  VERIFICAR COMO IMPORTAR A MENSAGEM DO PACKAGE ###

from dronecontrol2.msg import Vector3D


def publicaVelocity(x, y, z):
    pub = rospy.Publisher('controle/velocity', Vector3D, queue_size=10)
    rospy.init_node('joystick', anonymous=True)
    rate = rospy.Rate(10)  # 10hz
    vec = Vector3D()
    vec.x = x
    vec.y = y
    vec.z = z

    while not rospy.is_shutdown():
        rospy.loginfo(vec)
        pub.publish(vec)
        rate.sleep()


def publicaPosition(x, y, z):

    pub = rospy.Publisher('controle/position', Vector3D, queue_size=10)

    rospy.init_node('joystick', anonymous=True)
    rate = rospy.Rate(10)  # 10hz
    vec = Vector3D()
    vec.x = x
    vec.y = y
    vec.z = z

    while not rospy.is_shutdown():
        rospy.loginfo(vec)
        pub.publish(vec)
        rate.sleep()
# def pegaPose(msg):
#
#    posX = msg.pose.position.x
#    posY = msg.pose.position.y
#    posZ = msg.pose.position.z
#    print(1)

# def subscriber():

#    msg = PoseStamped()
#    rospy.Subscriber("controle/position", PoseStamped, pegaPose(msg))
#    rospy.spin()


if __name__ == '__main__':

    try:

        #msg = PoseStamped()

        #rospy.Subscriber('controle/position', PoseStamped, pegaPose(msg))

        #        subscriber()
        #publicaVelocity(10, 0, 0)
        publicaPosition(10, 0, 2)
        print("Welcome to Drone Joystick! Press -h to help you! Provided by Skyrats (Polytechnic School of University of Sao Paulo)")
###
    except rospy.ROSInterruptException:

        pass
