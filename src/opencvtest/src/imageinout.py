#!/usr/bin/env python

from __future__ import print_function

import mavros
import sys
import rospy
import cv2
from std_msgs.msg import String
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
import numpy as np
import argparse
import time


class image_converter:

  def __init__(self):
    self.image_pub = rospy.Publisher("image_topic_2", Image, queue_size=10)

    self.bridge = CvBridge()
    self.image_sub = rospy.Subscriber(
        "/camera1/image_raw", Image, self.callback)

  def callback(self, data):
    try:
      cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
    except CvBridgeError as e:
      print(e)

    (rows, cols, channels) = cv_image.shape
    if cols > 60 and rows > 60:
      cv2.circle(cv_image, (50, 50), 10, 255)

    find_lines(cv_image)
    cv2.imshow("Original topic window", cv_image)
    #find_circle(cv_image)
    cv2.waitKey(1)
    try:

      self.image_pub.publish(self.bridge.cv2_to_imgmsg(cv_image, "bgr8"))
    except CvBridgeError as e:
      print(e)


def find_lines(img_in):
  img = img_in.copy()
  gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
  edges = cv2.Canny(gray, 50, 150, apertureSize=3)

  lines = cv2.HoughLines(edges, 1, np.pi / 180, 200)
  for rho, theta in lines[0]:
    a = np.cos(theta)
    b = np.sin(theta)
    x0 = a * rho
    y0 = b * rho
    x1 = int(x0 + 1000 * (-b))
    y1 = int(y0 + 1000 * (a))
    x2 = int(x0 - 1000 * (-b))
    y2 = int(y0 - 1000 * (a))

    cv2.line(img, (x1, y1), (x2, y2), (0, 0, 255), 2)

  cv2.imshow('detected lines', img)


def find_circle(img_in):
  frame = img_in.copy()
  gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

  # apply GuassianBlur to reduce noise. medianBlur is also added for smoothening, reducing noise.
  #gray = cv2.GaussianBlur(gray, (5, 5), 0)
  circles = cv2.HoughCircles(
      gray, cv2.HOUGH_GRADIENT, 1, 260, param1=30, param2=65, minRadius=0, maxRadius=0)
  if(circles is None):
    return
  ar = np.around(circles)
  circles = np.uint16(ar)
  for i in circles[0, :]:
    # draw the outer circle
    cv2.circle(gray, (i[0], i[1]), i[2], (0, 255, 0), 2)
    # draw the center of the circle
    cv2.circle(gray, (i[0], i[1]), 2, (0, 0, 255), 3)

  cv2.imshow('detected circles', gray)


def main(args):
  ic = image_converter()
  rospy.init_node('image_converter', anonymous=True)

  try:
    rospy.spin()
  except KeyboardInterrupt:
    print("Shutting down")
  cv2.destroyAllWindows()


if __name__ == '__main__':
  main(sys.argv)
